﻿namespace HelloAngular.Models {
    export class Car {
        public Model: string;
        public Owner: string;

        constructor(model: string, owner: string) {
            this.Model = model;
            this.Owner = owner;
        }
    }
}