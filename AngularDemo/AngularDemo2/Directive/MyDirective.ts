﻿namespace HelloAngular.Directive {

    class MyDirective implements angular.IDirective {
        restrict = 'AE';
        templateUrl = 'Directive/MyDirective.html';
        replace = false;        
        scope = {
            person: "="
        }
        

        constructor() {
        }

        link = (scope: angular.IScope, element: angular.IAugmentedJQuery, attrs: angular.IAttributes, ctrl: any) => {
            
        }

        static factory(): angular.IDirectiveFactory {
            const directive = () => new MyDirective();
            directive.$inject = [];
            return directive;
        }
    }

    app.directive('myDirective', MyDirective.factory());
}

