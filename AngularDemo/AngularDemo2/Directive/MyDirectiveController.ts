﻿namespace HelloAngular.Controllers {
    "use strict";

    interface IMyDirectiveScope {
        person : string;
    }

    class MyDirectiveController {
        public static $inject = [
            "$scope"
        ];

        private binding: string;
        private PersonName: string;
        
        constructor(private $scope: IMyDirectiveScope) {
            this.binding = "This is our binding, which is awesome";

            this.PersonName = this.$scope.person;
        }
    }

    app.controller("MyDirectiveController", MyDirectiveController);
}