﻿
namespace HelloAngular.Controllers {
    "use strict";

    

    class RepeatDemoController {
        public static $inject = [];

        private headline: string;

        private Cars = [
            new Models.Car("Toyota", "Casper"),
            new Models.Car("Audi", "Jesper"),
            new Models.Car("Peugeot", "Christian"),
            new Models.Car("Skoda", "Anders"),
            new Models.Car("Audi", "Peter"),
            new Models.Car("Skoda", "Hans"),
            new Models.Car("Toyota", "Troel"),
            new Models.Car("VW", "Trine"),
            new Models.Car("Skoda", "Lars")
        ];

        constructor() {
            this.headline = "Hi Travelmarket!";
        }
    }

    app.controller("RepeatDemoController", RepeatDemoController);
}