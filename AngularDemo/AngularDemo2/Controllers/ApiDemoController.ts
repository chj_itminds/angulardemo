﻿namespace HelloAngular.Controllers {
    "use strict";

    class ApiDemoController {
        public static $inject = [
            "TodoApiService"
        ];

        private headline: string;
        private Todo: Models.Todo;

        private Todos: Array<Models.Todo>;
        
        constructor(private TodoApiService: Services.Api.TodoApiService) {
            
            this.TodoApiService.Get("1").then(result => {
                this.Todo = result[0];
            });

            this.TodoApiService.List().then(results => {
                this.Todos = results;
            });

        }

        
    }

    app.controller("ApiDemoController", ApiDemoController);
}