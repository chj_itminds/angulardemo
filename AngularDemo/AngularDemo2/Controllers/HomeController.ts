﻿namespace HelloAngular.Controllers {
    "use strict";


    class HomeController {
        public static $inject = [];

        private headline: string;

        constructor() {
            this.headline = "Hi Travelmarket!";
        }
    }

    app.controller("HomeController", HomeController);
}