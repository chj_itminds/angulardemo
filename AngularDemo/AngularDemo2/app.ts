﻿namespace HelloAngular {
    export var app = angular.module("helloAngular", ["foundation", "ui.router"]);

    class Config {
        public static inject = [
            "$stateProvider",
            "$urlRouterProvider"
        ];

        constructor(private $stateProvider: angular.ui.IStateProvider, private $urlRouterProvider: angular.ui.IUrlRouterProvider) {
            $stateProvider.state({
                name: "home",
                url: "/",
                templateUrl: "/Controllers/Home.html",
                controller: "HomeController",
                controllerAs: "vm"
            });

            $stateProvider.state({
                name: "directive",
                url: "/directive",
                templateUrl: "/Controllers/DirectiveDemo.html",
                //controller: "DirectiveDemoController",
                //controllerAs: "vm"
            });

            $stateProvider.state({
                name: "repeat",
                url: "/repeat",
                templateUrl: "/Controllers/RepeatDemo.html",
                //controller: "RepeatDemoController",
                //controllerAs: "vm"
            });

            $stateProvider.state({
                name: "api",
                url: "/api",
                templateUrl: "/Controllers/ApiDemo.html",
                //controller: "ApiDemoController",
                //controllerAs: "vm"
            });

            $urlRouterProvider.otherwise("/");
        }
    }

    app.config(Config);
}