﻿namespace HelloAngular.Services.Api {
    export class TodoApiService extends BaseApiService<Models.Todo> {
        public static $inject = [
            "$http",
            "$q"
        ];

        constructor(protected $http: angular.IHttpService, protected $q: angular.IQService) {
            super($http, $q, "Todos");
        }
    }

    app.service("TodoApiService", TodoApiService);
}