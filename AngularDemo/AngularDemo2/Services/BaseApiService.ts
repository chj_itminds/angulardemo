﻿namespace HelloAngular.Services.Api {
    export abstract class BaseApiService<TDataType> {
        protected BaseUrl = "http://jsonplaceholder.typicode.com";

        constructor(protected $http: angular.IHttpService, protected $q: angular.IQService, private controller: string) {

        }

        public Get(id: string, options?: angular.IRequestShortcutConfig): angular.IPromise<TDataType> {
            return this.WrapGet("", { id: id }, options);
        }

        public List(options?: angular.IRequestShortcutConfig): angular.IPromise<Array<TDataType>> {
            return this.$http.get(this.BuildUrl(""), options).then((response) => {
                return response.data;
            });
        }

        public Post(data: TDataType, options?: angular.IRequestShortcutConfig): angular.IPromise<TDataType> {
            return this.WrapPost("", data, options);
        }

        public Put(data: TDataType, options?: angular.IRequestShortcutConfig): angular.IPromise<TDataType> {
            return this.WrapPut("", data, "", options);
        }

        public Delete(id: string, options?: angular.IRequestShortcutConfig) {
            return this.WrapDelete("", id, options);
        }

        protected WrapGet(method: string, params: Object, options: angular.IRequestShortcutConfig = {}): angular.IPromise<TDataType> {
            return this.$http.get(this.BuildUrlWithQuery(method, params), options).then((response) => {
                return response.data;
            });
        }

        protected WrapList(method: string, params: Object, options: angular.IRequestShortcutConfig = {}): angular.IPromise<Array<TDataType>> {
            return this.$http.get(this.BuildUrlWithQuery(method, params), options).then((response) => {
                return response.data;
            });
        }

        protected WrapPost(method: string, data: any, dataInUri?: any, options: angular.IRequestShortcutConfig = {}): angular.IPromise<TDataType> {
            let uri: string;

            if (dataInUri !== undefined) {
                uri = this.BuildUrlWithQuery(method, dataInUri);
            } else {
                uri = this.BuildUrl(method);
            }

            return this.$http.post(uri, data, options).then((response) => {
                return response.data;
            });
        }

        protected WrapPut(method: string, data: any, dataInUri?: any, options: angular.IRequestShortcutConfig = {}): angular.IPromise<TDataType> {
            let uri: string;

            if (dataInUri !== undefined) {
                uri = this.BuildUrlWithQuery(method, dataInUri);
            } else {
                uri = this.BuildUrl(method);
            }

            return this.$http.put(uri, data, options).then((response) => {
                return response.data;
            });
        }

        protected WrapDelete(method: string, id: string, options: angular.IRequestShortcutConfig = {}): angular.IPromise<TDataType> {
            return this.$http.delete(this.BuildUrl(method, id), options).then((response) => {
                return response.data;
            });
        }

        protected BuildUrl(method: string, primaryKey?: string) {
            return `${this.BaseUrl}/${this.controller}/${method || primaryKey || ""}`;
        }

        protected BuildUrlWithQuery(method: string, params: Object) {
            var url = `${this.BaseUrl}/${this.controller}/${method}`;

            if (params !== undefined && params !== null) {
                for (let key in params) {
                    if (params.hasOwnProperty(key)) {
                        url = `${url}?${key}=${params[key]}`;
                    }
                }
            }

            return url;
        }
    }
}